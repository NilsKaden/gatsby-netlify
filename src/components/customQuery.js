import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

const CustomQuery = () => (
  <StaticQuery
    query={graphql`
      query {
        site {
          siteMetadata {
            info
          }
        }
      }
    `}
    render={data => <div><p>{data.site.siteMetadata.info}</p></div>}

  />
);

export default CustomQuery;