import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

const Blog = () => (
  <StaticQuery
    query={graphql`
      {
        allMarkdownRemark(
          filter: {
            frontmatter: { title: { eq: "christmastime" } }
          }
        ) {
          edges {
            node {
              html
              helper: frontmatter {
                title
                path
                date
              }
            }
          }
        }
      }
    `}
    render={data => <p>{data.allMarkdownRemark.edges[0].node.helper.title}</p>}
  />
);

export default Blog;